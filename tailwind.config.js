module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./node_modules/@themesberg/flowbite/**/*.js"
  ],
  theme: {
    fontFamily: {
      "montserrat": ["Montserrat", "Arial"]
    },
    extend: {},
  },
  plugins: [
    require("@themesberg/flowbite/plugin")
  ],
}
