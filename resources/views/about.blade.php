@extends('app.layout')

@section('title', 'About')

@section('meta', 'Hey! I am a 20 year old computer science and mathematics student. Here you can find my portfolio and projects I\'ve been working on.')

@section('content')
    <h1 class="text-3xl font-bold text-indigo-500">About</h1>
@endsection