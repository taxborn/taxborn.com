@extends('app.layout')

@section('title', 'Research')

@section('content')
    <h1 class="mt-8 text-3xl font-bold text-center text-gray-800 dark:text-gray-200 font-montserrat">Honors</h1>
    <h2 class="mb-4 text-2xl font-medium text-center text-gray-600 dark:text-gray-300 font-montserrat">Research, Scholarly, and Creative Activity</h2>

    <!-- Quote -->
    <div class="px-5 py-5 mx-auto w-full text-gray-800 bg-white rounded-lg shadow-lg lg:w-1/2 dark:bg-gray-800">
        <div class="h-3 text-3xl leading-tight text-left text-indigo-500">“</div>
        <p class="px-5 text-sm font-bold text-center text-gray-600 dark:text-gray-300">Students will access and utilize credible information to answer research questions. Students will complete and present an undergraduate research, scholarly, or creative project as part of their competency development. </p>
        <div class="-mt-3 h-3 text-3xl leading-tight text-right text-indigo-500">”</div>
    </div>

    <div class="grid grid-cols-1 gap-4 my-4">
        <div class="px-6 py-4 w-full bg-white rounded-lg shadow-md dark:bg-gray-800">
            <h5 class="title">Political Party Assignment <span class="text-sm italic text-gray-400">(To change)</span></h5>
            <p class="my-4 text">
                During my first semester at MSU, Mankato, I took a political science class. After becoming more 
                aware of the social and political situation surrounding me in high school, I wanted to learn more of the 
                foundational parts of our democracy so I could better understand the country I live in. One of the assignments 
                during this class was to analyze 3 political parties. One of which we thought we aligned with, one we thought 
                we did not align with, and one we haven't really heard much of before.
            </p>

            <a href="{{ asset('upload/research-reflection-development.pdf') }}" class="card-button">
                Read my reflection
                @include('app.svg.arrow-right-small')
            </a>

            <a href="{{ asset('upload/political-party-assignment.pdf') }}" class="card-button">
                Read the paper
                @include('app.svg.arrow-right-small')
            </a>
        </div> 
    </div>
@endsection