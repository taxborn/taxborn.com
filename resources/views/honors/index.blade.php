@extends('app.layout')

@section('title', 'Honors')

@section('content')
    <h1 class="mt-8 mb-2 text-3xl font-bold text-center text-gray-800 dark:text-gray-200 font-montserrat">Honors</h1>

    <!-- Quote -->
    <div class="px-5 py-5 mx-auto w-full text-gray-800 bg-white rounded-lg shadow-lg lg:w-1/2 dark:bg-gray-800">
        <div class="h-3 text-3xl leading-tight text-left text-indigo-500">“</div>
        <p class="px-5 text-sm font-bold text-center text-gray-600 dark:text-gray-300">The Honors Program at Minnesota State University, Mankato is committed to supporting motivated undergraduate students by providing them with exceptional learning opportunities, mentoring relationships, and a supportive community that fosters their personal, academic, and professional development.</p>
        <div class="-mt-3 h-3 text-3xl leading-tight text-right text-indigo-500">”</div>
    </div>

    <div class="grid grid-cols-1 gap-4 my-4 tracking-tight md:grid-cols-3 font-montserrat">
        <div>
            <a href="{{ route('honors.leadership') }}" class="block px-5 py-2.5 mr-2 mb-2 w-full text-sm font-medium text-center text-white bg-emerald-500 rounded-lg hover:bg-emerald-600 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-800">Leadership</a>
        </div>
        <div>
            <a href="{{ route('honors.research') }}" class="block px-5 py-2.5 mr-2 mb-2 w-full text-sm font-medium text-center text-white bg-rose-500 rounded-lg hover:bg-rose-600 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-800">Research</a>
        </div>
        <div>
            <a href="{{ route('honors.int-eng') }}" class="block px-5 py-2.5 mr-2 mb-2 w-full text-sm font-medium text-center text-white bg-indigo-500 rounded-lg hover:bg-indigo-600 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-800">Intercultural Engagement</a>
        </div>
    </div>

    <div class="grid grid-cols-1 gap-4 mb-4">
        <div class="px-6 py-4 w-full bg-white rounded-lg shadow-md dark:bg-gray-800">
            <h5 class="title">Why Honors?</h5>
            <p class="my-4 text">
                For my HONR-375 class, we wrote a reflection on why we are a part of the honors program. Also a part of the 
                reflection was why are still in the program. Here was my two page reflection on the question.
            </p>

            <a href="{{ asset('upload/why-honors.pdf') }}" class="card-button">
                Read
                @include('app.svg.arrow-right-small')
            </a>
        </div> 

        <div class="px-6 py-4 w-full bg-white rounded-lg shadow-md dark:bg-gray-800">
            <h5 class="title">Honors Competency Log</h5>
            <p class="my-4 text">
                In the time a student is in the honors program, they are required to complete 2 experiences in each competency, 
                both with development and application with respect to the competenct. On top of those 6 experiences, we need to 
                complete 2 suppliemental experiences. This log tracks where I am in the program and helps plan out my future with 
                the program.
            </p>

            <a href="{{ asset('upload/braxton-competency-log.pdf') }}" class="card-button">
                Read
                @include('app.svg.arrow-right-small')
            </a>
        </div> 
    </div>
@endsection