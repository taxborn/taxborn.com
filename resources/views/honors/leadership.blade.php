@extends('app.layout')

@section('title', 'Leadership')

@section('content')
    <h1 class="mt-8 text-3xl font-bold text-center text-gray-800 dark:text-gray-200 font-montserrat">Honors</h1>
    <h2 class="mb-4 text-2xl font-medium text-center text-gray-600 dark:text-gray-300 font-montserrat">Leadership</h2>

    <!-- Quote -->
    <div class="px-5 py-5 mx-auto w-full text-gray-800 bg-white rounded-lg shadow-lg lg:w-1/2 dark:bg-gray-800">
        <div class="h-3 text-3xl leading-tight text-left text-indigo-500">“</div>
        <p class="px-5 text-sm font-bold text-center text-gray-600 dark:text-gray-300">Students will utilize personal leadership values and strengths in a team environment toward a common goal. </p>
        <div class="-mt-3 h-3 text-3xl leading-tight text-right text-indigo-500">”</div>
    </div>

    <div class="grid grid-cols-1 gap-4 my-4">
        <div class="px-6 py-4 w-full bg-white rounded-lg shadow-md dark:bg-gray-800">
            <h5 class="title">Leading my team at Crocs</h5>
            <p class="my-4 text">
                From September 2018 to April 2021, I worked at Crocs at my local outlet mall. What initially I thought I was going to 
                write off as a job I didn't really care about in high school, turned into a two and a half year employment that was harder 
                than I thought to leave. I learned a lot from this job, such as the value of working in a team, how to communicate well, and 
                how to delegate tasks to get what needs to get done, done.
            </p>

            <a href="{{ asset('upload/leadership-reflection-development.pdf') }}" class="card-button">
                Read my reflection
                @include('app.svg.arrow-right-small')
            </a>
        </div> 
    </div>
@endsection