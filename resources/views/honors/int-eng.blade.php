@extends('app.layout')

@section('title', 'Intercultural Engagement')

@section('content')
    <h1 class="mt-8 text-3xl font-bold text-center text-gray-800 dark:text-gray-200 font-montserrat">Honors</h1>
    <h2 class="mb-4 text-2xl font-medium text-center text-gray-600 dark:text-gray-300 font-montserrat">Intercultural Engagement</h2>

    <!-- Quote -->
    <div class="px-5 py-5 mx-auto w-full text-gray-800 bg-white rounded-lg shadow-lg lg:w-1/2 dark:bg-gray-800">
        <div class="h-3 text-3xl leading-tight text-left text-indigo-500">“</div>
        <p class="px-5 text-sm font-bold text-center text-gray-600 dark:text-gray-300">Students will learn more about their own culture and develop an understanding of different cultural perspectives, ultimately establishing their own framework for intercultural engagement. </p>
        <div class="-mt-3 h-3 text-3xl leading-tight text-right text-indigo-500">”</div>
    </div>

    <div class="grid grid-cols-1 gap-4 my-4">
        <div class="px-6 py-4 w-full bg-white rounded-lg shadow-md dark:bg-gray-800">
            <h5 class="title">ISHA Yoga Immersion <span class="text-sm italic text-gray-400">(To change)</span></h5>
            <p class="my-4 text">
                During the summer of 2016, my family and I took a trip to Tennessee for a 
                youth program in meditation with the ISHA foundation. During this trip I was 
                immersed in an eastern culture, with pracitcies, cuisine, and social interations 
                that were unlike any experience I have had before.
            </p>

            <a href="{{ asset('upload/intercultural-engagement-development.pdf') }}" class="card-button">
                Read my reflection
                @include('app.svg.arrow-right-small')
            </a>
        </div> 
    </div>
@endsection