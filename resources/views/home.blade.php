@extends('app.layout')

@section('title', 'Home')

@section('meta', 'Hey! I am a 20 year old computer science and mathematics student. Here you can find my portfolio and projects I\'ve been working on.')

@section('content')
    <div class="flex flex-col items-center my-8 bg-white rounded-lg shadow-md md:flex-row dark:bg-gray-800">
        <img class="object-cover w-full h-96 rounded-t-lg md:h-128 md:w-64 md:rounded-none md:rounded-l-lg" src="{{ asset('img/profile.webp') }}" alt="">

        <div class="flex flex-col p-4 leading-normal">
            <h2 class="mb-1 title">Hey, I'm Braxton!</h2>
            <p class="mb-3 text">
                I am a 20 year old mathematics and computer science student at Minnesota State University, Mankato. My academic interest areas include operating 
                systems and compiler development, and more recreationally learning mathematics and logic. My expected graduation date is May of 2024, with 
                <span class="italic">(hopefully)</span> a future in a graduate studies program.
            </p>

            <h3 class="mb-1 title">My mission statement</h3>
            <p class="text">
                As a MSU, Mankato honors student, I want to learn how to take my academic skills learned pursing my degree in mathematics and computer science, 
                and utilize them in a productive, inclusive, and effective manner. I will achieve this goal by furthering my abilities in research, leadership, 
                and intercultural engagement. 
            </p>

            <div class="inline-flex justify-center mt-8 rounded-md sm:justify-start" role="group">
                <a href="https://www.linkedin.com/in/braxtonfair/" class="button left">
                    @include('app.svg.linkedin')
                    LinkedIn
                </a>
                <a href="{{ asset('upload/Resume.pdf') }}" target="_blank" data-tooltip-target="resume" class="button middle">
                    @include('app.svg.resume')
                    Resume
                </a>

                <div id="resume" role="tooltip" class="inline-block absolute invisible z-10 px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700">
                    This will bring you to a PDF version of my resume.
                    <div class="tooltip-arrow" data-popper-arrow></div>
                </div>

                <a href="https://gitlab.com/taxborn" class="button right">
                    @include('app.svg.gitlab')
                    GitLab
                </a>
            </div>
        </div>
    </div>
@endsection

@section('honors')
    <div class="p-8 mt-8 mb-4 w-full text-center bg-gray-800 border-t-4 border-indigo-600 shadow-md sm:p-6 honors">
        <h3 class="title-inverted">Looking for my honors portfolio?</h3>
        <p class="my-2 sm:mb-8 text-inverted">
            I'm a part of the Honors program at MSU, Mankato. Under this program, I work on my research, leadership, 
            and intercultural engagement skills. You can click the link below to see my portfolio of the program!
        </p>

        <div class="justify-center items-center space-y-4 sm:flex sm:space-y-0 sm:space-x-4">
            <a href="{{ route('honors.index') }}" class="flex inline-flex justify-center items-center px-4 py-2.5 w-full text-white bg-indigo-500 rounded-lg md:w-auto hover:bg-indigo-600 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-700">
                My portfolio
            </a>
        </div>
    </div>
@endsection

@section('projects')
    <div class="container px-2 mx-auto mb-0 md:mb-16">
        <h2 class="mb-4 text-3xl font-bold text-gray-800 dark:text-white">Projects</h2>

        <div class="grid grid-cols-1 gap-4 md:grid-cols-2">
            <div class="p-6 w-full bg-white rounded-lg shadow-md dark:bg-gray-800">
                <h5 class="title">taxborn.com</h5>
                <p class="text">
                    This website was coded from the ground up by me! I used <a href="https://laravel.com" class="link">Laravel</a>, 
                    <a href="https://tailwindcss.com" class="link">tailwindcss</a> to build the website, 
                    <a href="https://forge.laravel.com" class="link">Laravel Forge</a> to deploy it, and used 
                    <a href="https://m.do.co/c/a9684b1c6c81" class="link">Digital Ocean</a> as my hosting provider. 
                    This website is used as a place for me to host my projects that I build, and as a portfolio for the 
                    <a href="https://mankato.mnsu.edu/honors/" class="link">Honors Program</a> that I am a part of at MSU, Mankato.</p>
                <a href="https://gitlab.com/taxborn/taxborn.com" class="mt-2 card-button">
                    <span class="mr-1">Project</span>
                    @include('app.svg.arrow-right-small')
                </a>
            </div> 

            <div class="p-6 w-full bg-white rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
                <h5 class="title">hyperchessrs</h5>
                <p class="text">
                    This was a project that I submitted for a final in one of my computer science classes. I wrote an 
                    interpreter in Rust for the esoteric programming language, <a href="https://esolangs.org/wiki/4DChess" class="link">4DChess</a>. 
                    This was to practice Rust, a programming lanuage I've been learning, and to create an interpreter 
                    for a language that did not have one yet.
                </p>
                <a href="https://gitlab.com/taxborn/hyperchessrs" class="mt-2 card-button">
                    <span class="mr-1">Project</span>
                    @include('app.svg.arrow-right-small')
                </a>
            </div>

            <div class="p-6 w-full bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                <a href="https://en.wikipedia.org/wiki/Mastermind_(board_game)" target="_blank">
                <h5 class="title">
                    Mastermind
                    @include('app.svg.external-link')
                </h5>
                </a>
                <p class="text">
                    Initailly an in-class project that was built in my Data Structures course, I really liked the project 
                    and worked on the clone outside of class more to incorporate OOP principles and make it more robust. I 
                    used Visual Studio and the C# programming language here.
                </p>
                <a href="https://gitlab.com/taxborn/mastermind" class="mt-2 card-button">
                    <span class="mr-1">Project</span>
                    @include('app.svg.arrow-right-small')
                </a>

            </div>

            <div class="flex justify-center items-center">
                <h3 class="my-4 text-gray-600 dark:text-gray-400">... with more coming soon!</h3>
            </div>
        </div>
    </div>
@endsection