<nav class="px-2 py-2.5 bg-white rounded border-gray-200 shadow sm:px-4 dark:bg-gray-800 font-montserrat">
    <div class="container flex flex-wrap justify-between items-center mx-auto">
        <a href="{{ route('home') }}" class="flex">
            @include('app.svg.academic-cap')

            <span class="self-center text-lg font-semibold whitespace-nowrap dark:text-white">Braxton Fair</span>
        </a>

        <div class="flex sm:order-2">
            <!-- Honors Button -->
            <a href="{{ route('honors.index') }}" class="hidden items-center px-5 py-2.5 text-sm font-medium text-center text-white bg-indigo-500 rounded-lg sm:inline-flex hover:bg-indigo-600 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-800">
                Honors
                @include('app.svg.arrow-right-small')
            </a>
            
            <!-- Dark/Light mode toggle -->
            <button id="theme-toggle" type="button" aria-label="Dark and Light mode toggle" class="p-2.5 mx-2 text-sm text-gray-500 rounded-lg dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-700 focus:outline-none focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700">
                @include('app.svg.toggle-dark')
                @include('app.svg.toggle-light')
            </button>

            <!-- Hamburger menu -->
            <button data-collapse-toggle="mobile-menu-4" type="button" class="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg sm:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="mobile-menu-4" aria-expanded="false">
                <span class="sr-only">Open main menu</span>
                @include('app.svg.hamburger')
            </button>
        </div>

        <div class="hidden justify-between items-center w-full sm:flex sm:w-auto sm:order-1" id="mobile-menu-4">
            <ul class="flex flex-col mt-4 sm:flex-row sm:space-x-2 sm:mt-0 sm:text-sm sm:font-medium">
                <li>
                    <a href="{{ route('home') }}" class="navbar-item {{ Route::currentRouteName() == 'home' ? 'active' : 'inactive' }}" {{ Route::currentRouteName() == 'home' ? 'aria-current=page' : '' }} >Home</a>
                </li>

                <li>
                    <a href="{{ route('about') }}" class="navbar-item {{ Route::currentRouteName() == 'about' ? 'active' : 'inactive' }}" {{ Route::currentRouteName() == 'about' ? 'aria-current=page' : '' }}>About</a>
                </li>

                <li class="block sm:hidden">
                    <a href="{{ route('honors.index') }}" class="navbar-item {{ Route::currentRouteName() == 'honors.index' ? 'active' : 'inactive' }}" {{ Route::currentRouteName() == 'honors.index' ? 'aria-current=page' : '' }}>Honors</a>
                </li>
            </ul>
        </div>
    </div>
</nav>