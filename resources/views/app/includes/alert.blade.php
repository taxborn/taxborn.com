<div class="flex p-4 mx-auto mt-2 mb-4 max-w-screen-xl text-sm text-blue-700 bg-blue-100 rounded-lg shadow dark:bg-blue-200 dark:text-blue-800" role="alert">
    @include('app.svg.info')

    <div>
        <span class="font-bold">Heads up!</span> This website will be getting lots of updates over the weekend, lots of links <span class="underline">will</span> be broken,
        and things might look off for a bit. Most of this construction should be wrapped up around <span class="font-bold">February 1st, 2022</span>.
    </div>
</div>