<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="@yield('meta')">

        <title>Braxton Fair | @yield('title') | taxborn.com</title>

        <!-- CSS -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <!-- Manage dark/light mode toggle -->
        <script>
            // On page load or when changing themes, best to add inline in `head` to avoid FOUC
            if (localStorage.getItem('color-theme') === 'dark' || 
                (!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) 
            {
                document.documentElement.classList.add('dark');
            } else {
                document.documentElement.classList.remove('dark')
            }
        </script>
    </head>
    <body class="bg-gray-100 dark:bg-gray-700">
        @include('app.includes.nav')

        <div class="container px-2 mx-auto">
            @yield('content')
        </div>

        @yield('honors')
        @yield('projects')

        @include('app.includes.footer')

        <!-- Javascript -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </body>
</html>
