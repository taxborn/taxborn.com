<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'home')->name('home');

Route::view('/about', 'about')->name('about');

Route::view('/honors', 'honors.index')->name('honors.index');
Route::view('/honors/leadership', 'honors.leadership')->name('honors.leadership');
Route::view('/honors/research', 'honors.research')->name('honors.research');
Route::view('/honors/intercultural-engagement', 'honors.int-eng')->name('honors.int-eng');

